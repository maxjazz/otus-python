#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import collections
import datetime
import gzip
import json
import logging
import math
import os
import re


from collections import namedtuple

# log_format ui_short '$remote_addr  $remote_user $http_x_real_ip [$time_local] "$request" '
#                     '$status $body_bytes_sent "$http_referer" '
#                     '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER" '
#                     '$request_time';

LOG_FILE_PATTERN = re.compile('nginx-access-ui\.log-(\d{8})(?:\.(gz))?')
LOG_DATE_PATTERN = '%Y%m%d'
REPORT_DATE_PATTERN = "%Y.%m.%d"
REPORT_TEMPLATE_FILE = "report.html" 
LOG_LINE_PATTERN = re.compile('(.+)\s'      # :0  remote_addr
                              '(.+)\s'      # :1  remote_user
                              '(.+)\s'      # :2  http_x_real_ip
                              '\[(.+)\]\s'  # :3  time_local
                              '"(.+)"\s'    # :4  request
                              '(\d+)\s'     # :5  status
                              '(\d+)\s'     # :6  body_bytes_sent
                              '"(.+)"\s'    # :7  http_referer
                              '"(.+)"\s'    # :8  http_user_agent
                              '"(.+)"\s'    # :9  http_x_forwarded_for
                              '"(.+)"\s'    # :10 http_X_REQUEST_ID
                              '"(.+)"\s'    # :11 http_X_RB_USER
                              '(.+)\s',     # :12 request_time
                              re.UNICODE)

LogEntry = namedtuple('LogEntry', ['logfile', 'date'])

config = {
    "REPORT_SIZE" : 1000,
    "REPORT_DIR"  : "./reports",
    "LOG_DIR"     : "./log"
}

def parse_args():
    parser = argparse.ArgumentParser(description = 'Web Server Log Analyzer')
    parser.add_argument('-c', '--config', default = '',  
                        help = 'Path to config file ' )
    return parser.parse_args()

def parse_config(config_file):
    if not os.path.exists(config_file):
    
        return {}
    try:
        with open(config_file) as f:
            return json.load(f)
    except ValueError as e:
        raise Exception ('Could not parse config file', e)

def validate_config(config):
    if not os.path.exists(config['LOG_DIR']):
        raise Exception ('Could not find log directory %s' %config['LOG_DIR'])

def configure_log(logdir):
    if not logdir:
        logfile = './parser.log'
    else: 
        logfile = os.path.join (logdir, 'parser.log')
    
    config_args = {
            'level' : logging.INFO,
            'format': '[%(asctime)s] %(levelname).1s %(message)s',
            'dateformat' : '%Y.%m.%d %H.%M.%S',
            'filename'   : logfile
            }
    logging.basicConfig(**config_args)

def get_log_time(logfile):
    date_str, gzip_ext = LOG_FILE_PATTERN.match(logfile).groups()
    return datetime.datetime.strptime(date_str, LOG_DATE_PATTERN)

def get_log_type(logfile):
    date_str, gzip_ext = LOG_FILE_PATTERN.match(logfile).groups()
    if gzip_ext:
        return 'gzip'
    return 'plaintext'

def get_last_log(logdir):
    LAST_TIME = None
    LAST_LOG  = None
    logging.info ('Log directory is %s ' %logdir)
    for f in os.listdir(logdir):
        logfile = LOG_FILE_PATTERN.match(f)
        if logfile:
            logtime = get_log_time(f)
            if not LAST_TIME or logtime > LAST_TIME:
                LAST_TIME = logtime
                LAST_LOG  = f
    logging.info ('Last logfile have time %s' % LAST_TIME)
    logging.info ('Logfile name is %s' % LAST_LOG)
    return LogEntry(os.path.join(logdir, LAST_LOG), LAST_TIME)

def logread(log_path):
    if log_path.endswith('gz'):
        log = gzip.open(log_path,'r')
    else:
        log = open(log_path)
    for line in log:
        if line:
            yield line
    log.close()

def parse_line(line):
    g = LOG_LINE_PATTERN.match(line)
    if g:
        col_names = ('remote_addr', 
                     'remote_user', 
                     'http_x_real_ip',
                     'time_local',
                     'request_url', 
                     'status',
                     'body_bytes_sent',
                     'http_referer',
                     'http_user_agent',
                     'http_x_forwarded_for',
                     'http_X_REQUEST_ID',
                     'http_X_RB_USER',
                     'request_time')
        parsed_line = (dict(zip(col_names, g.groups())))
        parsed_line['request_time'] = float(parsed_line['request_time']) if parsed_line['request_time'] != '-' else 0
        return parsed_line
    return None

def get_report(log_stat, total_count, total_time, limit=100):
    report_data = []
    one_count_percent = float(total_count / 100)
    one_time_percent = float(total_time / 100)

    for url, times in log_stat.items():
        count = len(times)
        time_sum = sum(times)
        report_data.append({
            'url': url,
            'time_max': max(times),
            'count': count,
            'time_sum': round_f(time_sum),
            'count_perc': round_f(count / one_count_percent),
            'time_perc': round_f(time_sum / one_time_percent),
            'median': median(times)
            })
    report_data.sort(key=lambda x: (x['time_perc'], x['time_sum']), reverse=True)

    return report_data[:limit]

def percentile(lst, p):
    lst = sorted(lst)
    index = (p / 100.0) * len(lst)
    if math.floor(index) == index:
        result = (lst[int(index)-1] + lst[int(index)]) / 2.0
    else:
        result = lst[int(math.floor(index))]
    return result


def median(lst):
    lst = sorted(lst)
    n = len(lst)
    if n == 0:
        result = 0
    elif n % 2 == 1:
        result = lst[n//2]
    else:
        result = (lst[n//2-1] + lst[n//2]) / 2.0
    return result


def round_f(number):
    return round(number, 3)

def run_analyze(log):
    report_path   = "%s/report-%s.html" % (config['REPORT_DIR'], log.date.strftime(REPORT_DATE_PATTERN))
    template_path = os.path.join(config['REPORT_DIR'], REPORT_TEMPLATE_FILE)
    if not os.path.exists(config['REPORT_DIR']):
        os.mkdir(config['REPORT_DIR']) 
    if os.path.exists(report_path):
        logging.info('Report file %s already exist' %report_path)
        exit(0)
    log_stat = collections.defaultdict(list)
    total_count = total_time = 0
    for line in logread(log.logfile):
        parsed_line = parse_line(line)
        if parsed_line:
            total_count += 1
            total_time  += parsed_line['request_time']
            log_stat[parsed_line['request_url']].append(parsed_line['request_time'])
    if total_count > 0 and total_time > 0:
        log_report = get_report(log_stat, total_count, total_time, config['REPORT_SIZE'])
        save_report(log_report, report_path)
        logging.info ('Report file `%s` is ready!' % report_path)
    else:
        logging.info ( 'Log `%s` data is empty or has incorrect data' % log.logfile )

def save_report(report, file_path):
    if file_path.endswith('.html'):
        with open('./report.html', 'r') as f:
            file_data = f.read()
        file_data = file_data.replace('$table_json', json.dumps(report))
        with open(file_path, 'w') as f:
            f.write(file_data)
    elif file_path.endswith('.json'):
        with open(file_path, 'w') as f:
            json.dump(report, f)
    else:
        raise RuntimeError('Unexpected report file format')

def main():
    args = parse_args()

    loaded_config = parse_config(args.config)
    config.update(loaded_config)
    parser_log_dir = config.get('PARSER_LOG_DIR', None)
    configure_log(parser_log_dir)
    validate_config(config)
    log = get_last_log(config['LOG_DIR'])
    logging.info ('Logfile name is %s' % log.logfile)
    run_analyze(log)


if __name__ == "__main__":
    main()
